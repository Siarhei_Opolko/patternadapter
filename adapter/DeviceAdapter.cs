﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public class DeviceAdapter : IRussainStandart
    {
        public string Name
        {
            get { return _europeanStandartDevice.Description; }
            set { }
        }

        private readonly IEuropeanStandart _europeanStandartDevice;
        public DeviceAdapter(IEuropeanStandart europeanStandartDevice)
        {
            _europeanStandartDevice = europeanStandartDevice;
        }

        public void Connect()
        {
            _europeanStandartDevice.TurnOn();
        }

        public void Disconnect()
        {
            _europeanStandartDevice.TurnOff();
        }
    }
}

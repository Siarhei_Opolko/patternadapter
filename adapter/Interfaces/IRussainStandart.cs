﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public interface IRussainStandart
    {
        string Name { get; set; }
        void Connect();
        void Disconnect();
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public interface IEuropeanStandart
    {
        string Description { get; }
        void TurnOn();
        void TurnOff();
    }
}

﻿using System;

namespace Adapter
{
    class Program
    {
        static void Main(string[] args)
        {
            IRussainStandart russianDevice = new RussianElectricalPlug("RussianDeviceElectricPlug");
            DisplayInfoDevice.DisplayInfo(russianDevice);

            IEuropeanStandart europeanDevice = new EuropeanElectricPlug("EuropeanDeviceElectricPlug");

            IRussainStandart adapter = new DeviceAdapter(europeanDevice);
            DisplayInfoDevice.DisplayInfo(adapter);

            Console.ReadLine();
        }
    }
}

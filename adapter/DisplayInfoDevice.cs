﻿using System;

namespace Adapter
{
    public static class DisplayInfoDevice
    {
        public static void DisplayInfo(IRussainStandart russianDeviceStandart)
        {
            Console.WriteLine("Мое описание:{0}",russianDeviceStandart.Name);
            Console.WriteLine("Я подключаюсь таким образом:");russianDeviceStandart.Connect();
            Console.WriteLine("Я отключаюсь таким образом:");russianDeviceStandart.Disconnect();
            Console.WriteLine();
        }
    }
}

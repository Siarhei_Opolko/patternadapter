﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Adapter
{
    public class RussianElectricalPlug : IRussainStandart
    {
        public string Name { get; set; }

        public RussianElectricalPlug(string name)
        {
            Name = name;
        }
        public void Connect()
        {
            Console.WriteLine("Мне нужно напряжение 220В");
        }

        public void Disconnect()
        {
            Console.WriteLine("Выдерните меня из розетки");
        }
    }
}

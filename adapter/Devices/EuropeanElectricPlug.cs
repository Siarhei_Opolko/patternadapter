﻿using System;

namespace Adapter
{
    public class EuropeanElectricPlug : IEuropeanStandart
    {
        public string Description { get; private set; }

        public EuropeanElectricPlug(string description)
        {
            Description = description;
        }

        public void TurnOn()
        {
            Console.WriteLine("Подключите меня к напряжению 320В");
        }

        public void TurnOff()
        {
            Console.WriteLine("Нажмите на кнопку OFF");
        }
    }
}
